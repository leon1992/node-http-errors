HTTP Errors
===========


Install
-------

```bash
npm install @lleon/http-errors
```

Example
-------

### Test instance

```javascript
import { HttpError, NotFound } from '@lleon/http-errors';

const error = new NotFound()

console.log(error instanceof Error)     // true
console.log(error instanceof HttpError) // true
console.log(error instanceof NotFound)  // true
```


### With default options

```javascript
import { NotFound } from '@lleon/http-errors';

const error = new NotFound();

console.log(error.statusCode); // 404
console.log(error.message);    // "Not Found"
console.log(error.toJSON());   // { "statusCode": 404, "message": "Not Found", "timestamp": 1512682132993, "metadata": {} }
```

### With custom error message

```javascript
import { NotFound } from '@lleon/http-errors';

const error = new NotFound('User not found');

console.log(error.statusCode); // 404
console.log(error.message);    // "User not found"
console.log(error.toJSON());   // { "statusCode": 404, "message": "User not Found", "timestamp": 1512682132993, "metadata": {} }
```

### With custom metadata

```javascript
import { NotFound } from '@lleon/http-errors';

const error = new NotFound({ id: 1 });

console.log(error.statusCode); // 404
console.log(error.message);    // "User not found"
console.log(error.toJSON());   // { "statusCode": 404, "message": "Not Found", "timestamp": 1512682132993, "metadata": { "id": 1 } }
```

### With custom message and custom metadata

```javascript
import { NotFound } from '@lleon/http-errors';

const error = new NotFound('User not found', { id: 1 });

console.log(error.statusCode); // 404
console.log(error.message); // "User not found"
console.log(error.toJSON()); // { "statusCode": 404, "message": "User not Found", "timestamp": 1512682132993, "metadata": { "id": 1 } }
```

Errors
------

|               Error               | Code |              Message               |
| --------------------------------- | ---- | ---------------------------------- |
| `BadRequest`                      | 400  | Bad Request                        |
| `Unauthorized`                    | 401  | Unauthorized                       |
| `PaymentRequired`                 | 402  | Payment Required                   |
| `Forbidden`                       | 403  | Forbidden                          |
| `NotFound`                        | 404  | Not Found                          |
| `MethodNotAllowed`                | 405  | Method Not Allowed                 |
| `NotAcceptable`                   | 406  | Not Acceptable                     |
| `ProxyAuthenticationRequired`     | 407  | Proxy Authentication Required      |
| `RequestTimeout`                  | 408  | Request Timeout                    |
| `Conflict`                        | 409  | Conflict                           |
| `Gone`                            | 410  | Gone                               |
| `LengthRequired`                  | 411  | Length Required                    |
| `PreconditionFailed`              | 412  | Precondition Failed                |
| `PayloadTooLarge`                 | 413  | Payload Too Large                  |
| `RequestUriTooLong`               | 414  | Request-URI Too Long               |
| `UnsupportedMediaType`            | 415  | Unsupported Media Type             |
| `RequestedRangeNotSatisfiable`    | 416  | Requested Range Not Satisfiable    |
| `ExpectationFailed`               | 417  | Expectation Failed                 |
| `ImATeapot`                       | 418  | I'm a teapot                       |
| `MisdirectedRequest`              | 421  | Misdirected Request                |
| `UnprocessableEntity`             | 422  | Unprocessable Entity               |
| `Locked`                          | 423  | Locked                             |
| `FailedDependency`                | 424  | Failed Dependency                  |
| `UpgradeRequired`                 | 426  | Upgrade Required                   |
| `PreconditionRequired`            | 428  | Precondition Required              |
| `TooManyRequests`                 | 429  | Too Many Requests                  |
| `RequestHeaderFieldsTooLarge`     | 431  | Request Header Fields Too Large    |
| `ConnectionClosedWithoutResponse` | 444  | Connection Closed Without Response |
| `UnavailableForLegalReasons`      | 451  | Unavailable For Legal Reasons      |
| `ClientClosedRequest`             | 499  | Client Closed Request              |
| `InternalServerError`             | 500  | Internal Server Error              |
| `NotImplemented`                  | 501  | Not Implemented                    |
| `BadGateway`                      | 502  | Bad Gateway                        |
| `ServiceUnavailable`              | 503  | Service Unavailable                |
| `GatewayTimeout`                  | 504  | Gateway Timeout                    |
| `HttpVersionNotSupported`         | 505  | HTTP Version Not Supported         |
| `VariantAlsoNegotiates`           | 506  | Variant Also Negotiates            |
| `InsufficientStorage`             | 507  | Insufficient Storage               |
| `LoopDetected`                    | 508  | Loop Detected                      |
| `NotExtended`                     | 510  | Not Extended                       |
| `NetworkAuthenticationRequired`   | 511  | Network Authentication Required    |
| `NetworkConnectTimeoutError`      | 599  | Network Connect Timeout Error      |
