import { expect } from 'chai';
import * as faker from 'faker';
import * as sinon from 'sinon';

import { HttpError, TooManyRequests } from '../../src';

const sandbox = sinon.sandbox.create();

describe('TooManyRequests', function() {
  afterEach(() => sandbox.restore());

  describe('#constructor()', function() {
    it('should create an error', function() {
      const error = new TooManyRequests();

      expect(error).to.be.instanceOf(Error);
      expect(error).to.be.instanceOf(HttpError);
      expect(error).to.be.instanceOf(TooManyRequests);

      expect(error.metadata).to.be.deep.equal({});
      expect(error.message).to.be.equal('Too Many Requests');
      expect(error.status).to.be.equal(429);
      expect(error.statusCode).to.be.equal(429);
      expect(error.name).to.be.equal('TooManyRequests');
      expect(error.timestamp).to.be.instanceOf(Date);
    });

    it('should create an error with custom metadata', async function() {
      const metadata = faker.helpers.userCard();
      const error = new TooManyRequests(metadata);

      expect(error.metadata).to.be.deep.equal(metadata);
    });

    it('should create an error with a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new TooManyRequests(message);

      expect(error.message).to.be.equal(message);
    });

    it('should create an error with custom message and custom metadata', async function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();
      const error = new TooManyRequests(message, metadata);

      expect(error.message).to.be.equal(message);
      expect(error.metadata).to.be.deep.equal(metadata);
    });
  });

  describe('#valueOf()', function() {
    it('should be equal to the status code', function() {
      const error = new TooManyRequests();

      expect(error.valueOf()).to.be.equal(error.status);
      expect(error.valueOf()).to.be.equal(error.statusCode);
    });
  });

  describe('#toString()', function() {
    it('should be equal to the default message', function() {
      const error = new TooManyRequests();

      expect(error.toString()).to.be.equal('Too Many Requests');
    });

    it('should be equal to a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new TooManyRequests(message);

      expect(error.toString()).to.be.equal(message);
    });
  });

  describe('#toJSON()', function() {
    it('should match the error data', function() {
      const error = new TooManyRequests();
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
    it('should match the error data with custom data', function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();

      const error = new TooManyRequests(message, metadata);
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });

    it('should return the correct data when calling `JSON.stringify`', function() {
      const error = new TooManyRequests();
      const json = JSON.parse(JSON.stringify(error));

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
  });
});
