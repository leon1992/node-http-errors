import { expect } from 'chai';
import * as faker from 'faker';
import * as sinon from 'sinon';

import { HttpError, InternalServerError } from '../../src';

const sandbox = sinon.sandbox.create();

describe('InternalServerError', function() {
  afterEach(() => sandbox.restore());

  describe('#constructor()', function() {
    it('should create an error', function() {
      const error = new InternalServerError();

      expect(error).to.be.instanceOf(Error);
      expect(error).to.be.instanceOf(HttpError);
      expect(error).to.be.instanceOf(InternalServerError);

      expect(error.metadata).to.be.deep.equal({});
      expect(error.message).to.be.equal('Internal Server Error');
      expect(error.status).to.be.equal(500);
      expect(error.statusCode).to.be.equal(500);
      expect(error.name).to.be.equal('InternalServerError');
      expect(error.timestamp).to.be.instanceOf(Date);
    });

    it('should create an error with custom metadata', async function() {
      const metadata = faker.helpers.userCard();
      const error = new InternalServerError(metadata);

      expect(error.metadata).to.be.deep.equal(metadata);
    });

    it('should create an error with a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new InternalServerError(message);

      expect(error.message).to.be.equal(message);
    });

    it('should create an error with custom message and custom metadata', async function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();
      const error = new InternalServerError(message, metadata);

      expect(error.message).to.be.equal(message);
      expect(error.metadata).to.be.deep.equal(metadata);
    });
  });

  describe('#valueOf()', function() {
    it('should be equal to the status code', function() {
      const error = new InternalServerError();

      expect(error.valueOf()).to.be.equal(error.status);
      expect(error.valueOf()).to.be.equal(error.statusCode);
    });
  });

  describe('#toString()', function() {
    it('should be equal to the default message', function() {
      const error = new InternalServerError();

      expect(error.toString()).to.be.equal('Internal Server Error');
    });

    it('should be equal to a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new InternalServerError(message);

      expect(error.toString()).to.be.equal(message);
    });
  });

  describe('#toJSON()', function() {
    it('should match the error data', function() {
      const error = new InternalServerError();
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
    it('should match the error data with custom data', function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();

      const error = new InternalServerError(message, metadata);
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });

    it('should return the correct data when calling `JSON.stringify`', function() {
      const error = new InternalServerError();
      const json = JSON.parse(JSON.stringify(error));

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
  });
});
