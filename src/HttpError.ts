import { HttpStatus, HttpStatusMessage } from '@lleon/http-status';

import { HttpErrorJson } from './HttpErrorJson';

export abstract class HttpError<T = any> extends Error {
  abstract statusCode: HttpStatus;

  readonly timestamp: Date;
  readonly metadata?: T;

  constructor(message?: string);
  constructor(metadata?: T);
  constructor(message?: string, metadata?: T);
  constructor(message?: any, metadata?: T) {
    if (typeof message !== 'string') {
      metadata = message;
      message = undefined;
    }

    super(message);

    if (typeof message !== 'undefined') {
      Object.defineProperty(this, 'message', { value: message });
    }

    this.timestamp = new Date();
    this.metadata = typeof metadata !== 'undefined' ? metadata : ({} as any);
  }

  get message() {
    return HttpStatusMessage[this.statusCode];
  }

  get name() {
    return this.constructor.name;
  }

  get status(): number {
    return this.statusCode;
  }

  valueOf(): number {
    return this.statusCode;
  }

  /**
   * String reprsentation of the error
   */
  toString(): string {
    return this.message;
  }

  /**
   * JSON representation of the error
   */
  toJSON(): HttpErrorJson<T> {
    return {
      message: this.message,
      statusCode: this.statusCode,
      timestamp: this.timestamp.getTime(),
      metadata: this.metadata
    };
  }
}
