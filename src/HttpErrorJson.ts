import { HttpStatus } from '@lleon/http-status';

export interface HttpErrorJson<T = any> {
  message: string;
  statusCode: HttpStatus;
  timestamp: number;
  metadata: T;
}
