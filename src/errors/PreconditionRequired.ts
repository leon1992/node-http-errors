import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class PreconditionRequired<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.PreconditionRequired;
}
