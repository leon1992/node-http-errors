import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class LoopDetected<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.LoopDetected;
}
