import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class TooManyRequests<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.TooManyRequests;
}
