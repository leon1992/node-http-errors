import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class ConnectionClosedWithoutResponse<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.ConnectionClosedWithoutResponse;
}
