import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class UnprocessableEntity<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.UnprocessableEntity;
}
