import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class MethodNotAllowed<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.MethodNotAllowed;
}
