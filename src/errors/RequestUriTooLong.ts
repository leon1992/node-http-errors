import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class RequestUriTooLong<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.RequestUriTooLong;
}
