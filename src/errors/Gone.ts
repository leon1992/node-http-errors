import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class Gone<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.Gone;
}
