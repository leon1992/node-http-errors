import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class InternalServerError<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.InternalServerError;
}
