import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class NetworkAuthenticationRequired<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.NetworkAuthenticationRequired;
}
