import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class VariantAlsoNegotiates<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.VariantAlsoNegotiates;
}
