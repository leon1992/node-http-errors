import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class PayloadTooLarge<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.PayloadTooLarge;
}
