import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class ExpectationFailed<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.ExpectationFailed;
}
