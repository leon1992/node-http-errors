import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class UnsupportedMediaType<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.UnsupportedMediaType;
}
