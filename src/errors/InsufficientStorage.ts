import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class InsufficientStorage<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.InsufficientStorage;
}
