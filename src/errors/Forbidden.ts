import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class Forbidden<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.Forbidden;
}
