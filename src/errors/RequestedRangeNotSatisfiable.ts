import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class RequestedRangeNotSatisfiable<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.RequestedRangeNotSatisfiable;
}
