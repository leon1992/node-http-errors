import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class PreconditionFailed<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.PreconditionFailed;
}
