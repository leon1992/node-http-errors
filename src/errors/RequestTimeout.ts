import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class RequestTimeout<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.RequestTimeout;
}
