import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class LengthRequired<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.LengthRequired;
}
