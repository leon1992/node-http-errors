import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class Locked<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.Locked;
}
