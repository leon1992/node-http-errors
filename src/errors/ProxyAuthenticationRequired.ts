import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class ProxyAuthenticationRequired<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.ProxyAuthenticationRequired;
}
