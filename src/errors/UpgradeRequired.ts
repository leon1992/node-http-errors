import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class UpgradeRequired<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.UpgradeRequired;
}
