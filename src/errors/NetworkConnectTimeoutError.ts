import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class NetworkConnectTimeoutError<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.NetworkConnectTimeoutError;
}
