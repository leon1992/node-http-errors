import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class Conflict<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.Conflict;
}
