const fs = require('fs');
const path = require('path');
const { HttpStatus } = require('@lleon/http-status');

function getContents(error) {
  return `import { HttpStatus } from '@lleon/http-status';

import { HttpError } from '../HttpError';

export class ${error}<T = any> extends HttpError<T> {
  readonly statusCode = HttpStatus.${error};
}
`;
}

function getDestination(name) {
  return path.join(__dirname, `../src/errors/${name}.ts`);
}

const index = Object.entries(HttpStatus)
  .filter(([error, code]) => {
    return typeof code === 'number' && code >= 400 && !/^\d+$/.test(error.toString());
  })
  .map(([error, code]) => error)
  .map(error => {
    const contents = getContents(error);
    const destination = getDestination(error);

    fs.writeFileSync(destination, contents);

    return error;
  })
  .reduce((previous, current) => {
    return previous + `export { ${current} } from './${current}';\n`;
  }, '');

fs.writeFileSync(getDestination('index'), index);
