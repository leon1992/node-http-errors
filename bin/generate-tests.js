const fs = require('fs-extra');
const path = require('path');
const { HttpStatus, HttpStatusMessage } = require('@lleon/http-status');

function getDestination(name) {
  return path.join(__dirname, `../tests/errors/${name}.test.ts`);
}

fs.ensureDirSync(path.join(__dirname, '../tests/errors'));

const index = Object.entries(HttpStatus)
  .filter(([error, code]) => {
    return typeof code === 'number' && code >= 400 && !/^\d+$/.test(error.toString());
  })
  .map(([error, code]) => {
    const contents = getContents(error, code, HttpStatusMessage[code]);
    const destination = getDestination(error);

    fs.writeFileSync(destination, contents);

    return error;
  });

function getContents(errorName, errorCode, errorMessage) {
  errorMessage = errorMessage.replace(/\'/g, "\\'");

  return `import { expect } from 'chai';
import * as faker from 'faker';
import * as sinon from 'sinon';

import { HttpError, ${errorName} } from '../../src';

const sandbox = sinon.sandbox.create();

describe('${errorName}', function() {
  afterEach(() => sandbox.restore());

  describe('#constructor()', function() {
    it('should create an error', function() {
      const error = new ${errorName}();

      expect(error).to.be.instanceOf(Error);
      expect(error).to.be.instanceOf(HttpError);
      expect(error).to.be.instanceOf(${errorName});

      expect(error.metadata).to.be.deep.equal({});
      expect(error.message).to.be.equal('${errorMessage}');
      expect(error.status).to.be.equal(${errorCode});
      expect(error.statusCode).to.be.equal(${errorCode});
      expect(error.name).to.be.equal('${errorName}');
      expect(error.timestamp).to.be.instanceOf(Date);
    });

    it('should create an error with custom metadata', async function() {
      const metadata = faker.helpers.userCard();
      const error = new ${errorName}(metadata);

      expect(error.metadata).to.be.deep.equal(metadata);
    });

    it('should create an error with a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new ${errorName}(message);

      expect(error.message).to.be.equal(message);
    });

    it('should create an error with custom message and custom metadata', async function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();
      const error = new ${errorName}(message, metadata);

      expect(error.message).to.be.equal(message);
      expect(error.metadata).to.be.deep.equal(metadata);
    });
  });

  describe('#valueOf()', function() {
    it('should be equal to the status code', function() {
      const error = new ${errorName}();

      expect(error.valueOf()).to.be.equal(error.status);
      expect(error.valueOf()).to.be.equal(error.statusCode);
    });
  });

  describe('#toString()', function() {
    it('should be equal to the default message', function() {
      const error = new ${errorName}();

      expect(error.toString()).to.be.equal('${errorMessage}');
    });

    it('should be equal to a custom message', function() {
      const message = faker.lorem.paragraph();
      const error = new ${errorName}(message);

      expect(error.toString()).to.be.equal(message);
    });
  });

  describe('#toJSON()', function() {
    it('should match the error data', function() {
      const error = new ${errorName}();
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
    it('should match the error data with custom data', function() {
      const message = faker.lorem.paragraph();
      const metadata = faker.helpers.userCard();

      const error = new ${errorName}(message, metadata);
      const json = error.toJSON();

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });

    it('should return the correct data when calling \`JSON.stringify\`', function() {
      const error = new ${errorName}();
      const json = JSON.parse(JSON.stringify(error));

      expect(json.message).to.be.equal(error.message);
      expect(json.statusCode).to.be.equal(error.statusCode);
      expect(json.statusCode).to.be.equal(error.status);
      expect(json.timestamp).to.be.equal(error.timestamp.getTime());
      expect(json.metadata).to.be.deep.equal(error.metadata);
    });
  });
});
`;
}
